package main

import (
    "gitlab.com/colourdelete/recipes/pkg/server"
    "log"
)

func main() {
	log.Panic(recipes.server.Run())
}
